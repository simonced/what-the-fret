
/* chords */

// ref: https://www.fender.com/pages/guitar-chords
// each chord note is an array: [string (1 the higher), fret (0 is open), finger]
// fingers:
// - 1 thumb (rarely used is it?)
// - 2 index
// - 3 middle
// - 4 ring
// - 5 pinky

// Open positions
const chords = {
	"C": [
		[6, 0, "X"],
		[5, 3, "3"], // C
		[4, 2, "2"], // E
		[3, 0, "O"], // G
		[2, 1, "1"], // C
		[1, 0, "O"], // E
	],
	// ref: https://onlineguitarbooks.com/c-sharp-chord/
	"Cs-tri": [
		[1, 4, "1"], // G#
		[2, 6, "4"], // F
		[3, 6, "3"], // C# (root)
	],
	"Cs-tri1": [
		[1, 9, "1"],  // C#
		[2, 9, "1"],  // G#
		[3, 10, "2"], // F
	],
	"Cs-tri2": [
		[1, 1, "1"], // F
		[2, 2, "3"], // C#
		[3, 1, "2"], // G#
	],
	"D": [
		[6, 0, "X"],
		[5, 0, "X"],
		[4, 0, "O"], // D
		[3, 2, "1"], // A
		[2, 3, "3"], // D
		[1, 2, "2"], // F#
	],
	"Dm": [
		[6, 0, "X"],
		[5, 0, "X"],
		[4, 0, "O"], // D
		[3, 2, "2"], // A
		[2, 3, "3"], // D
		[1, 1, "1"], // F
	],
	"E": [
		[6, 0, "O"], // E
		[5, 2, "2"], // B
		[4, 2, "3"], // E
		[3, 1, "1"], // G#
		[2, 0, "O"], // B
		[1, 0, "O"], // E
	],
	"Em": [
		[6, 0, "O"], // E
		[5, 2, "2"], // B
		[4, 2, "3"], // E
		[3, 0, "O"], // G
		[2, 0, "O"], // B
		[1, 0, "O"], // E
	],
	"F": [
		[6, 0, "X"],
		[5, 0, "X"],
		[4, 3, "3"], // F
		[3, 2, "2"], // A
		[2, 1, "1"], // C
		[1, 1, "1"], // F
	],
	"G": [
		[6, 3, "2"], // G
		[5, 2, "1"], // B
		[4, 0, "O"], // D
		[3, 0, "O"], // G
		[2, 0, "O"], // B
		[1, 3, "4"], // G
	],
	"A": [
		[6, 0, "X"],
		[5, 0, "O"], // B
		[4, 2, "1"], // E
		[3, 2, "2"], // A
		[2, 2, "3"], // C#
		[1, 0, "O"], // G
	],
	"Am": [
		[6, 0, "X"],
		[5, 0, "O"], // A
		[4, 2, "2"], // E
		[3, 2, "3"], // A
		[2, 1, "1"], // C
		[1, 0, "O"], // E
	],
	"B": [
		[6, 0, "X"],
		[5, 2, "1"], // B
		[4, 4, "2"], // F#
		[3, 4, "3"], // B
		[2, 4, "4"], // D#
		[1, 0, "X"],
	],
	"Bm": [
		[6, 0, "X"],
		[5, 2, "1"], // B
		[4, 0, "X"],
		[3, 4, "4"], // B
		[2, 3, "3"], // D
		[1, 2, "2"], // F#
	],
};

// setChord("G"); // DBG

/**
 * Displays a chord
 * @param {array[array]} chord_ see chords constant above
 */
function setChord(chord_) {
	clearAll();
	this.event.target.classList.add("selected");
	chords[chord_].forEach(setNote);
}
