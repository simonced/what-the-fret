document.querySelectorAll(".fret-note").forEach(x => {
	let note = x.getAttribute("data-note");

	// by default, hide sharp notes
	if(note.includes("s")) {
		x.classList.add("hidden");
	}

	// add events when selecting notes (for manual selection mode)
	x.addEventListener("click", fretNoteSelection);
});

document.querySelectorAll("button.drawer-button").forEach(x => {
	x.addEventListener("click", toggleDrawer);
});


/**
 * Drawer toggle (on/off) button action
 * @param {Event} e_ clicked events
 */
function toggleDrawer(e_) {
	document.querySelector(this.getAttribute("data-target")).classList.toggle("closed");
}

/**
 * Hide all notes onthe fret board
 */
function hideNoteAll() {
	document.querySelectorAll(".fret-note").forEach(x => {
		x.classList.add("hidden");
	});
}


/**
 * Show all notes on the fret board
 */
function showNoteAll() {
	document.querySelectorAll(".fret-note").forEach(x => {
		x.classList.remove("hidden");
	});
}


/**
 * Piano key action
 * @param {string} note_ note to show/hide/toggle
 */
function pianoNote(note_) {
	let mode = document.querySelector("[name=piano-mode]:checked").getAttribute("value");
	// console.log( mode ); // DBG
	document.querySelectorAll(".fret-note").forEach(x => {
		let note = x.getAttribute("data-note");
		if(note!=note_) return;
		if(mode=="add") x.classList.remove("hidden");
		if(mode=="del") x.classList.add("hidden");
		if(mode=="toggle") x.classList.toggle("hidden");
	});
}


/**
 * Activate all the same notes on the fret board
 * @param {string} note_ A or As (A#)
 */
function toggleNote(note_) {
	document.querySelectorAll(".fret-note").forEach(x => {
		let note = x.getAttribute("data-note");
		if(note==note_) {
			x.classList.toggle("hidden");
		}
	});
}


function manualSelection() {
	let fb = document.querySelector(".fret-board");
	if(fb.classList.contains("editing")) {
		// stop editing
		fb.classList.remove("editing");
	}
	else {
		// start editing
		fb.classList.add("editing");
	}
}

function clearSelection() {
	document.querySelectorAll(".fret-note").forEach(x => {
		x.classList.remove("selected");
	});
}


/**
 * Event Listener of notes selection (on the fret board)
 * only when in editing mode
 */
function fretNoteSelection(e_) {
	if(document.querySelector(".fret-board").classList.contains("editing")==false) return;
	this.classList.toggle("selected");
}


/* === reset features === */

/**
 * clear out everything:
 * - buttons
 * - selected statuses
 * - notes
 * - annotations
 */
function clearAll() {
	hideNoteAll();
	resetChordButtons();
	removeAnnotations();
}


/**
 * clear chord buttons selected status
 */
function resetChordButtons() {
	// buttons
	document.querySelectorAll(".chord-button").forEach(x => {
		x.classList.remove("selected");
	});
}


/**
 * clear out all annotations
 */
function removeAnnotations() {
	document.querySelectorAll(".annotation").forEach(x => {
		x.remove();
	});
}


/**
 * Add annotation to note (for chords)
 * @param {HTMLElement} note_ the note to add annotation to
 * @param {string} anno_ the character to show
 */
function addAnnotation(note_, anno_) {
	let f = document.createElement("div");
	f.classList.add("annotation");
	f.innerText = anno_;
	note_.after(f);
}


/* === switch on notes (not used for manual editing) === */

const strings = document.querySelectorAll(".fret-board tr");

/**
 * Activate a note on the fret board given coordinates
 * Optionally adds an annotation
 * @param {Array} note_ [string, fret, annotation]
 */
function setNote(note_) {
	let [string, fret, annotation] = note_;
	// console.log(string, fret); // DBG
	let frets = strings[string-1].querySelectorAll("td");
	// console.log(frets); // DBG
	let n = frets[fret].querySelector(".fret-note");
	n.classList.remove("hidden");
	if(annotation) {
		addAnnotation(n, annotation);
	}
}


document.querySelectorAll("aside .tabs a").forEach(x => {
	x.addEventListener("click", y => {
		// console.log( y.target ); // DBG
		let tgt = y.target.getAttribute("href");
		switchTab(tgt);
	});
});

/**
 * Switch to a tab (in the options)
 * @param {string} tab_ query selector of the tab to show
 */
function switchTab(tab_) {
	// off everything
	document.querySelectorAll(".tabs li").forEach(x => {
		x.classList.remove("is-active");
	});
	document.querySelectorAll(".tab-content").forEach(x => {
		x.classList.remove("is-active");
	});

	// activate one tab
	let tab = document.querySelector(".tabs li a[href='"+tab_+"']");
	if(tab) {
		tab.parentElement.classList.add("is-active");
	}

	let tab2 = document.querySelector(tab_);
	if(tab2) {
		tab2.classList.add("is-active");
	}
}

switchTab("#tab-chords");

