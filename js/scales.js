
const scales = {
	"Co": [ // C open position
		[5, 3],
		[4, 0],
		[4, 2],
		[4, 3],
		[3, 0],
		[3, 2],
		[2, 0],
		[2, 1]
	],
	"C1": [ // C 1st position
		[5, 3],
		[5, 5],
		[4, 2],
		[4, 3],
		[4, 5],
		[3, 2],
		[3, 4],
		[3, 5]
	],
	"C4": [ // C 4th position
		[6, 8],
		[5, 5],
		[5, 7],
		[5, 8],
		[4, 5],
		[4, 7],
		[3, 4],
		[3, 5],
	],
	// next octave
	"C4-2": [
		[3, 5],
		[3, 7],
		[2, 5],
		[2, 6],
		[2, 8],
		[1, 5],
		[1, 7],
		[1, 8],
	],
	"C7": [ // C 7th position
		[6, 8],
		[6, 10],
		[5, 7],
		[5, 8],
		[5, 10],
		[4, 7],
		[4, 9],
		[4, 10],
	],
	"C7-2": [
		// next octave
		[4, 10],
		[3, 7],
		[3, 9],
		[3, 10],
		[2, 8],
		[2, 10],
		[1, 7],
		[1, 8]
	]
};

/**
 * Displays a Scale
 * @param {array[array]} scale_ see chords constant above
 */
function setScale(scale_) {
	clearAll();
	this.event.target.classList.add("selected");
	scales[scale_].forEach(setNote);
}